-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 11, 2020 at 09:52 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_money`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_menu`
--

CREATE TABLE `tb_menu` (
  `id` int(15) NOT NULL,
  `judul_menu` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `is_main_menu` int(15) NOT NULL,
  `icon` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_menu`
--

INSERT INTO `tb_menu` (`id`, `judul_menu`, `link`, `is_main_menu`, `icon`) VALUES
(1, 'Dashboard', '#', 0, ''),
(2, 'Dashboard', '#', 1, 'fas fa-fw fa-tachometer-alt'),
(3, 'Master', '#', 0, ''),
(4, 'Data Pemasukan', 'Pemasukan', 3, 'fas fa-fw fa-folder'),
(5, 'Data Pengeluaran', 'Pengeluaran', 3, 'fas fa-fw fa-folder'),
(6, 'Data Tabungan', 'Tabungan', 3, 'fas fa-fw fa-folder'),
(7, 'Data Sedekah', 'Sedekah', 3, 'fas fa-fw fa-folder');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pemasukan`
--

CREATE TABLE `tb_pemasukan` (
  `id` int(15) NOT NULL,
  `jumlah` int(99) NOT NULL,
  `dari` varchar(255) NOT NULL,
  `waktu` date NOT NULL,
  `user_id` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pemasukan`
--

INSERT INTO `tb_pemasukan` (`id`, `jumlah`, `dari`, `waktu`, `user_id`) VALUES
(1, 5000, 'Duma', '2020-06-09', 1),
(5, 50000, 'Mama', '2020-07-04', 1),
(6, 20000, 'sakdksa', '2020-07-07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengeluaran`
--

CREATE TABLE `tb_pengeluaran` (
  `id` int(15) NOT NULL,
  `nm_toko` varchar(50) NOT NULL,
  `nm_barang` varchar(255) NOT NULL,
  `qty` int(10) NOT NULL,
  `harga` int(99) NOT NULL,
  `total` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `user_id` int(15) NOT NULL,
  `type` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pengeluaran`
--

INSERT INTO `tb_pengeluaran` (`id`, `nm_toko`, `nm_barang`, `qty`, `harga`, `total`, `tgl`, `user_id`, `type`) VALUES
(1, 'Sanjaya', 'Mozaic', 1, 200000, 200000, '2020-06-24', 1, 0),
(5, 'sdas', 'dsadsa', 1, 20000, 20000, '2020-07-06', 1, 1),
(6, 'sdako', 'sdadasi', 2, 5000, 10000, '2020-07-07', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_sedekah`
--

CREATE TABLE `tb_sedekah` (
  `id` int(15) NOT NULL,
  `jumlah_sedekah` int(99) NOT NULL,
  `dari` varchar(50) NOT NULL,
  `untuk` varchar(50) NOT NULL,
  `tgl_sedekah` date NOT NULL,
  `user_id` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_sedekah`
--

INSERT INTO `tb_sedekah` (`id`, `jumlah_sedekah`, `dari`, `untuk`, `tgl_sedekah`, `user_id`) VALUES
(2, 5000, 'Mama', 'Pengemis', '2020-07-06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_tabungan`
--

CREATE TABLE `tb_tabungan` (
  `id` int(15) NOT NULL,
  `jumlah` int(99) NOT NULL,
  `saving_place` varchar(50) NOT NULL,
  `tgl_save` date NOT NULL,
  `user_id` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_tabungan`
--

INSERT INTO `tb_tabungan` (`id`, `jumlah`, `saving_place`, `tgl_save`, `user_id`) VALUES
(2, 20000, 'Celengan', '2020-07-06', 1),
(3, 10000, 'Celangan', '2020-08-06', 1),
(4, 50000, 'Celengan', '2020-09-07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(15) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `name`, `username`, `password`, `email`, `img`) VALUES
(1, 'Dumasari Ulfah Ramadhani Siahaan', 'dumasari', '$2y$10$WWzlahOz1Yt7Rr.kKHkyk.Bsfq/d8j0VTDZIn5dXzoDjxO1eURIRm', 'dumasariulfah@gmail.com', '1927.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_menu`
--
ALTER TABLE `tb_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pemasukan`
--
ALTER TABLE `tb_pemasukan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pengeluaran`
--
ALTER TABLE `tb_pengeluaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_sedekah`
--
ALTER TABLE `tb_sedekah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_tabungan`
--
ALTER TABLE `tb_tabungan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_menu`
--
ALTER TABLE `tb_menu`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_pemasukan`
--
ALTER TABLE `tb_pemasukan`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_pengeluaran`
--
ALTER TABLE `tb_pengeluaran`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_sedekah`
--
ALTER TABLE `tb_sedekah`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_tabungan`
--
ALTER TABLE `tb_tabungan`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
