<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <?php if (validation_errors()) : ?>
        <div class="alert alert-danger" role="alert">
            <?= validation_errors(); ?>
        </div>
    <?php endif; ?>
    <section class="content-header">
        <?php echo $this->session->flashdata('message'); ?>
    </section>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="pull-right">
                <a href="<?= site_url('theme/add'); ?>" class="btn btn-primary mb-1">Add Theme</a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover" id="theme-table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name Theme</th>
                            <th>Screen Shot 1</th>
                            <th>Screen Shot 2</th>
                            <th>Screen Shot 3</th>
                            <th>Category</th>
                            <th>File</th>
                            <th>WhatsApp</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var table;
    $(document).ready(function() {
        //datatables
        table = $('#theme-table').DataTable({
            "processing": true, //Feature control the processing indicator.
            "lengthChange": true,
            "serverSide": true,
            "order": [],
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('theme/theme'); ?>",
                "type": "POST"
            },
            "columnDefs": [{
                "targets": [-1],
                "orderable": false,
            }, ],

        });

        table.on('draw.dt', function(data, type, row) {
            var PageInfo = $('#theme-table').DataTable().page.info();
            table.column(0, {
                page: 'current'
            }).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            });
        });

    });


    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }


    function delete_theme(id) {
        $("#confirm").attr("action", '<?= base_url('theme/delete'); ?>/' + id);
    }
</script>

<form id="confirm" action="" method="POST">
    <div id="hapus" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Mohon Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <p>Apakah anda yakin akan menghapus data ini ?</p>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-danger" value="Ya"></button>
                    <button type="button" class="btn btn-light" data-dismiss="modal">Tidak</button>
                </div>
            </div>
        </div>
    </div>
</form>