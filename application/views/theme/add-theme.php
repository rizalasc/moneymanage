<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <?php if (validation_errors()) : ?>
        <div class="alert alert-danger" role="alert">
            <?= validation_errors(); ?>
        </div>
    <?php endif; ?>
    <section class="content-header">
        <?php echo $this->session->flashdata('message'); ?>
    </section>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $main_title; ?></h6>
        </div>
        <div class="card-body">
            <div class="col-md-6">
            <form method="POST" action="<?=base_url('theme/save');?>" enctype="multipart/form-data"> 
                <div class="form-group">
                    <label for="themeName">Theme Name</label>
                    <input type="text" class="form-control" name="themeName" id="themeName" placeholder="Enter Name of Theme">
                    <?= form_error('themeName', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>
                <div class="form-group">
                    <label for="themeCategory">Theme Category</label>
                    <select name="themeCategory" id="themeCategory" class="form-control required">
                        <option value="">- Select Category -</option>
                        <?php foreach($category as $c): ?>
                            <option value="<?=$c->id?>"><?=$c->category_name;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="fileTheme">File Theme</label>
                    <input type="file" class="form-control-file" id="fileTheme" name="fileTheme">
                </div>
                <div class="form-group">
                    <label for="whatsappType">WhatsApp Type</label>
                    <select name="whatsappType" id="whatsappType" class="form-control required">
                        <option value="">- Select Whatsapp -</option>
                        <?php foreach($whatsapp as $w): ?>
                            <option value="<?=$w->id?>"><?=$w->whatsapp;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="screenshot1">ScreenShot 1</label>
                    <input type="file" class="form-control-file" id="screenshot1" name="screenshot1">
                </div>
                <div class="form-group">
                    <label for="screenshot2">ScreenShot 2</label>
                    <input type="file" class="form-control-file" id="screenshot2" name="screenshot2">
                </div>
                <div class="form-group">
                    <label for="screenshot3">ScreenShot 3</label>
                    <input type="file" class="form-control-file" id="screenshot3" name="screenshot3">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>