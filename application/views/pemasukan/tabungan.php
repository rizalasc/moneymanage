<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <?php if (validation_errors()) : ?>
    <div class="alert alert-danger" role="alert">
        <?= validation_errors(); ?>
    </div>
    <?php endif; ?>
    <section class="content-header">
        <?php echo $this->session->flashdata('message'); ?>
    </section>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="pull-right">
                <a href="" class="btn btn-primary mb-1" data-toggle="modal" data-target="#newModalkategori">Tambah Tabungan</a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover" id="table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Jumlah</th>
                            <th>Tempat Simpan</th>
                            <th>Tanggal</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var table;
    $(document).ready(function() {
        //datatables
        table = $('#table').DataTable({
            "processing": true, //Feature control the processing indicator.
            "lengthChange": true,
            "serverSide": true,
            "order": [],
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('tabungan/datatable'); ?>",
                "type": "POST"
            },
            "columnDefs": [{
                "targets": [-1],
                "orderable": false,
            }, ],

        });

        table.on('draw.dt', function(data, type, row) {
            var PageInfo = $('#table').DataTable().page.info();
            table.column(0, {
                page: 'current'
            }).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            });
        });

        $('#newKategoriModalLabel').text('Tambah Tabungan'); // Set Title to Bootstrap modal title
        $('#form').attr('action', '<?= base_url('tabungan/add'); ?>');
    });


    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    function edit_tabungan(id) {
        save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('#form').attr('action', '<?= base_url('tabungan/update/'); ?>' + id);
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#newModalkategori').modal();

        $.ajax({
            url: "<?php echo site_url('tabungan/edit/') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {

                $('[name="id"]').val(data.id);
                $('[name="jumlah"]').val(data.jumlah);
                $('[name="save_place"]').val(data.saving_place);
                $('[name="tgl_save"]').val(data.tgl_save);

                $('#edit_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Tabungan'); // Set title to Bootstrap modal title

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_tabungan(id) {
        $("#confirm").attr("action", '<?= base_url('tabungan/delete'); ?>/' + id);
    }
</script>

<div class="modal fade" id="newModalkategori" tabindex="-1" role="dialog" aria-labelledby="newKategoriModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newKategoriModalLabel">Add New Sub Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="#" method="post" id="form">
                <div class="modal-body">
                    <input type="hidden" value="" name="id" />
                    <div class="form-group">
                        <label class="control-label col-md-12">Jumlah</label>
                        <div class="col-md-12">
                            <input name="jumlah" placeholder="Jumlah Uang" class="form-control" type="number">
                            <span class="help-block"></span>
                        </div>
                        <label class="control-label col-md-12">Tempat Simpan</label>
                        <div class="col-md-12">
                            <input name="save_place" placeholder="Tempat Simpan" class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                        <label class="control-label col-md-12">Tanggal</label>
                        <div class="col-md-12">
                            <input name="tgl_save" placeholder="Tanggal" class="form-control" type="date">
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<form id="confirm" action="" method="POST">
    <div id="hapus" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Mohon Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <p>Apakah anda yakin akan menghapus data ini ?</p>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-danger" value="Ya"></button>
                    <button type="button" class="btn btn-light" data-dismiss="modal">Tidak</button>
                </div>
            </div>
        </div>
    </div>
</form>
