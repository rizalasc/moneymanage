<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <div class="card shadow mb-4">
        <div class="container bootstrap snippet">
            <div class="row">
                <div class="col-sm-10 mb-3">
                    <h1></h1>
                </div>
            </div>
            <?= form_open_multipart('auth/edit'); ?>
            <div class="row">
                <div class="col-sm-3">
                    <!--left col-->
                    <div class="text-center">
                        <img src="<?php echo base_url('assets/img/profile/') . $user['img']; ?>" class="img-thumbnail">
                        <h6 class="mt-2">Upload a different photo...</h6>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="image" name="image">
                            <label class="custom-file-label" for="image">Max.1 MB</label>
                        </div>
                    </div>
                    <br>

                </div>
                <!--/col-3-->
                <div class="col-sm-9">
                    <div class="tab-content">
                        <div class="tab-pane active" id="home">
                            <hr>
                            <div class="form-group">

                                <div class="col-xs-6">
                                    <label for="name">
                                        <h4>Name</h4>
                                    </label>
                                    <input type="text" class="form-control" name="name" id="name" value="<?= $user['name']; ?>" placeholder="Enter your name....">
                                    <?= form_error('name', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-xs-6">
                                    <label for="name">
                                        <h4>Username</h4>
                                    </label>
                                    <input type="text" class="form-control" name="username" id="username" placeholder="Username" readonly value="<?= $user['username']; ?>">
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-xs-6">
                                    <label for="email">
                                        <h4>Email</h4>
                                    </label>
                                    <input type="text" class="form-control" name="email" id="email" value="<?= $user['email']; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-6">
                                    <br>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                            </form>

                            <hr>

                        </div>

                    </div>
                    <!--/tab-pane-->
                </div>
                <!--/tab-content-->

            </div>
            </form>
            <!--/col-9-->
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->
<script>
    $('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
    });
</script>