<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <?php if (validation_errors()) : ?>
    <div class="alert alert-danger" role="alert">
        <?= validation_errors(); ?>
    </div>
    <?php endif; ?>
    <section class="content-header">
        <?php echo $this->session->flashdata('message'); ?>
    </section>
    <div class="card shadow mb-4">
        <div class="card-header">
            <div class="card mb-3" style="max-width: 300px;">
                <div class="row no-gutters">
                    <!--Profile Card 3-->
                    <div class="col-md-12">
                        <div class="card profile-card-3">
                            <div class="background-block">
                                <img src="https://images.pexels.com/photos/459225/pexels-photo-459225.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" alt="profile-sample1" class="background" />
                            </div>
                            <div class="profile-thumb-block">
                                <img src="<?= base_url('assets/img/profile/') . $user['img']; ?>" alt="profile-image" class="profile" />
                            </div>
                            <div class="card-content">
                                <h2><?= $user['username']; ?>
                                    <small><?= $user['email']; ?>
                                    </small></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="editprofile" class="btn btn-primary">Edit</a>
        </div>
    </div>
</div>