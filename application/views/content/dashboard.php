<div class="container-fluid">
    <!-- Content Row -->
    <!-- Page Heading -->
    <section class="content-header">
        <?php echo $this->session->flashdata('message'); ?>
    </section>
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800"><?php echo $title; ?></h1>
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Pemasukan (<?=date('F');?>)</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php
                                if (count($info[0])>0) {
                                    echo "Rp. ".$info[0]["total_pemasukan"];
                                } else {
                                    echo "Rp. 0";
                                }
                                
                            ?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Pengeluaran (<?=date('F');?>)</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                            <?php
                            if (count($info[1])>0) {
                                echo "Rp. ".$info[1]["total_pengeluaran"];
                            } else {
                                echo "Rp. 0";
                            }
                            ?>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Tabungan (<?=date('F');?>)</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                            <?php
                            if (count($info[2])>0) {
                                echo "Rp. ".$info[2]["total_tabungan"];
                            } else {
                                echo "Rp. 0";
                            }
                            ?>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-basic text-uppercase mb-1">Sedekah (<?=date('F');?>)</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                            <?php
                            if (count($info[3])>0) {
                                echo "Rp. ".$info[3]["total_sedekah"];
                            } else {
                                echo "Rp. 0";
                            }
                            ?>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Content Row -->
    <div class="row">
        <div class="col-md-6" style="margin-bottom: 10px;">

            <form method="POST" id="filter-form" class="form-inline" role="form" action="<?= base_url('dashboard'); ?>">
                <div class="form-group mb-2 mx-2">
                    <select name="bulan" id="month" class="form-control">
                        <?php
                        $month = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
                        // var_dump(count($month));
                        for ($i=0; $i < count($month) ; $i++) { 
                            echo "<option value='" . ($i+1) . "'>" . $month[$i] . "</option>";
                        }
                            
                        ?>
                    </select>
                </div>
                <div class="form-group mb-2 mx-2">
                    <select class="form-control" name="tahun">
                        <?php
                        $year = date("Y");
                        for ($i = ($year-3); $i <= ($year+5); $i++) {
                            echo "<option value='" . $i . "'>" . $i . "</option>";
                        }
                        ?>
                    </select>
                </div>
                    <button type="submit" class="btn btn-primary mb-2">Filter</button>
            </form>
        </div>

        <div class="col-xl-7 col-lg-7">

            <!-- Area Chart -->
            <div class="card shadow mb-4">
                <!-- <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Bulanan</h6>
                </div> -->
                <div class="card-body" style="">
                    <!-- <div class="chart-area"> -->
                        <canvas id="canvas_bulanan"></canvas>
                    <!-- </div> -->
                    <!-- <hr> -->
                </div>
            </div>

        </div>

        <!-- Donut Chart -->
        <div class="col-md-5">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <canvas id="chart_pengeluaran"></canvas>
                </div>
            </div>
        </div>
        <div class="col-xl-7 col-lg-5">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <!-- Card Body -->
                <div class="card-body">
                        <canvas id="chart_bulanan"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
		var color = Chart.helpers.color;
		var barChartData = {
			labels: <?=$months;?>,
			datasets: [{
				label: 'Pemasukan',
				backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
				borderColor: window.chartColors.blue,
				borderWidth: 1,
				data: <?=$pemasukan;?>
			}, {
				label: 'Pengeluaran',
				backgroundColor: color(window.chartColors.green).alpha(0.5).rgbString(),
				borderColor: window.chartColors.green,
				borderWidth: 1,
				data: <?=$pengeluaran;?>
			}, {
				label: 'Tabungan',
				backgroundColor: color(window.chartColors.yellow).alpha(0.5).rgbString(),
				borderColor: window.chartColors.yellow,
				borderWidth: 1,
				data: <?=$tabungan;?>
			}, {
				label: 'Sedekah',
				backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
				borderColor: window.chartColors.red,
				borderWidth: 1,
				data: <?=$sedekah;?>
			}]

		};

		$(document).ready(function() {
			var ctx = document.getElementById('canvas_bulanan').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: 'Grafik Tahunan '+<?=$tahun;?>
					}
				}
			});
		});
	</script>
    <script>
        var color = Chart.helpers.color;
		var barBulanan = {
			datasets: [{
				label: 'Pemasukan',
				backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
				borderColor: window.chartColors.blue,
				borderWidth: 1,
				data: <?="[".$bulanan[0]."]";?>
			}, {
				label: 'Pengeluaran',
				backgroundColor: color(window.chartColors.green).alpha(0.5).rgbString(),
				borderColor: window.chartColors.green,
				borderWidth: 1,
				data: <?="[".$bulanan[1]."]";?>
			}, {
				label: 'Tabungan',
				backgroundColor: color(window.chartColors.yellow).alpha(0.5).rgbString(),
				borderColor: window.chartColors.yellow,
				borderWidth: 1,
				data: <?="[".$bulanan[2]."]";?>
			}, {
				label: 'Sedekah',
				backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
				borderColor: window.chartColors.red,
				borderWidth: 1,
				data: <?="[".$bulanan[3]."]";?>
			}]

		};

		$(document).ready(function() {
			var ctx = document.getElementById('chart_bulanan').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barBulanan,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: '<?="Grafik Bulanan ".$bulan." / ".$tahun."";?>'
					}
				}
			});
		});
    </script>
    <script>
        var config = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: <?=$tipe;?>,
                        backgroundColor: [
                            window.chartColors.orange,
                            window.chartColors.yellow,
                        ],
                        label: 'Dataset 1'
                    }],
                    labels: [
                        'Tetap',
                        'Tidak Tetap'
                    ]
                },
                options: {
                    responsive: true,
                    title:{
                        display:true,
                        text:'<?="Pengeluaran Bulanan Berdasarkan Tipe ".$bulan." / ".$tahun."";?>'
                    }
                }
            };
        $(document).ready(function(){
            var ctx = document.getElementById('chart_pengeluaran').getContext('2d');
			window.myPie = new Chart(ctx, config);
        });
    </script>