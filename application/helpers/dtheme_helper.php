<?php
function _sendEmail($email, $token, $type)

{
    $CI = &get_instance();

    $CI->load->library('email');

    $config = [

        'mailtype'  => 'html',

        'charset'   => 'utf-8',

        'protocol'  => 'smtp',

        'smtp_host' => 'ssl://smtp.googlemail.com',

        'smtp_user' => 'systemsiperus@gmail.com',    // Ganti dengan email gmail kamu

        'smtp_pass' => 'Rahasiapublic123',      // Password gmail kamu

        'smtp_port' => 465,

        'crlf'      => "\r\n",

        'newline'   => "\r\n"

    ];



    $CI->email->initialize($config);
    $CI->email->set_newline("\r\n");

    $CI->email->from('admin@dtheme.com', 'Dtheme Admin');

    $CI->email->to($email);


    if ($type == 'verify') {

        $data['title'] = "Verify your email address";
        $data['message'] = "Thanks for signing up for Siperus! We're excited to have you as an early admin";
        $data['button'] = "Verify Email";
        $data['link'] = base_url() . 'auth/verify?email=' . $email . '&token=' . urlencode($token);


        $CI->email->subject('Account Verification');
        $message = $CI->load->view('email/index', $data, true);

        $CI->email->message($message);
    } else if ($type == 'forgot') {
        $data['title'] = "Reset Password";
        $data['message'] = "We've got request from your account to reset your password";
        $data['button'] = "Reset Password";
        $data['link'] = base_url() . 'auth/resetpassword?' . $email . '&token=' . urlencode($token);

        $CI->email->subject('Reset Password');
        $message = $CI->load->view('email/index', $data, true);
        $CI->email->message($message);
    }



    if ($CI->email->send()) {

        return true;
    } else {

        echo $CI->email->print_debugger();
    }
}

function _layout($layout, $data)

{

    $CI = &get_instance();

    $CI->load->view('template/header', $data);

    $CI->load->view('template/sidebar', $data);

    $CI->load->view('template/topbar', $data);

    $CI->load->view($layout, $data);

    $CI->load->view('template/footer');
}

if (!function_exists('_uploadFile')) {
    function _uploadFile($nameField,$themeName)
    {
        $config_file['allowed_types']='zip';
        $config_file['max_size']='2048';
        $config_file['upload_path']='./upload/theme/'.str_replace(" ","",$themeName).'/';
        $config_file['file_name']=str_replace(" ","",$themeName);
        $config_file['overwrite']=true;
        $CI = &get_instance();
        $CI->load->library('upload');
        $CI->upload->initialize($config_file);
        if ($CI->upload->do_upload($nameField)) {
            return TRUE;
        } else {
            return FALSE;
        }
        
        // return $CI->upload->do_upload($nameField);
    }
}

if (!function_exists("_uploadImage")) {
    function _uploadImage($nameField,$themeName,$fileName){
        $config["allowed_types"]='jpeg|jpg|png';
        $config["max_size"]='512';
        $config['upload_path']='./upload/theme/'.str_replace(" ","",$themeName).'/'.$fileName;
        $config['file_name']=str_replace(" ","",$themeName);
        $config['overwrite']=true;
        $CI = &get_instance();
        $CI->load->library('upload');
        $CI->upload->initialize($config);
        if ($CI->upload->do_upload($nameField)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}

if (!function_exists("is_logged_in")) {
    function is_logged_in()
    {
        $CI =& get_instance();
        if (!$CI->session->userdata('username')) {
            
            redirect('auth','refresh');
            
        }
    }
}


