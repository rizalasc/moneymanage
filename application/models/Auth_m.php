<?php


defined('BASEPATH') or exit('No direct script access allowed');

class Auth_m extends CI_Model
{
    protected $table = 'tb_user';

    public function getUser($username)
    {
        return $this->db->get_where($this->table, ['username' => $username])->row_array();
    }
}

/* End of file Auth_m.php */
