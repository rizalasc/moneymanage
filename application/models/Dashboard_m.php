<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_m extends CI_Model {
    public function getInfo()
    {
        $month = date('m');
        $pemasukan = $this->db->select('(SELECT SUM(tb_pemasukan.jumlah) FROM tb_pemasukan WHERE user_id='.$this->session->userdata("id_user").' AND MONTH(tb_pemasukan.waktu)='.$month.') AS total_pemasukan',FALSE)->get('tb_pemasukan')->result_array();
        $pengeluaran = $this->db->select('(SELECT SUM(tb_pengeluaran.total) FROM tb_pengeluaran WHERE user_id='.$this->session->userdata("id_user").' AND MONTH(tb_pengeluaran.tgl)='.$month.') AS total_pengeluaran',FALSE)->get('tb_pengeluaran')->result_array();
        $tabungan = $this->db->select('(SELECT SUM(tb_tabungan.jumlah) FROM tb_tabungan WHERE user_id='.$this->session->userdata("id_user").' AND MONTH(tb_tabungan.tgl_save)='.$month.') AS total_tabungan',FALSE)->get('tb_tabungan')->result_array();
        $sedekah = $this->db->select('(SELECT SUM(tb_sedekah.jumlah_sedekah) FROM tb_sedekah WHERE user_id='.$this->session->userdata("id_user").' AND MONTH(tb_sedekah.tgl_sedekah)='.$month.') AS total_sedekah',FALSE)->get('tb_sedekah')->result_array();
        
        // print_r($pemasukan[0]);
        // echo "<br>";
        // print_r($pengeluaran[0]);
        // echo "<br>";
        // print_r($tabungan[0]);
        // echo "<br>";   
        // print_r($sedekah[0]);

        $data = array();
        $data[] = $pemasukan[0];
        $data[] = $pengeluaran[0];
        $data[] = $tabungan[0];
        $data[] = $sedekah[0];

        return $data;
    }

    public function getDataChart()
    {
        $pemasukan = $this->db->query("SELECT MONTH(tb_pemasukan.waktu) AS month FROM tb_pemasukan WHERE user_id=".$this->session->userdata('id_user')."")->result();
        $pengeluaran = $this->db->query("SELECT MONTH(tb_pengeluaran.tgl) AS month FROM tb_pengeluaran WHERE user_id=".$this->session->userdata('id_user')."")->result();
        $tabungan = $this->db->query("SELECT MONTH(tb_tabungan.tgl_save) AS month FROM tb_tabungan WHERE user_id=".$this->session->userdata('id_user')."")->result();
        $sedekah = $this->db->query("SELECT MONTH(tb_sedekah.tgl_sedekah) AS month FROM tb_sedekah WHERE user_id=".$this->session->userdata('id_user')."")->result();

        $query = array();
        $query[] = $pemasukan;
        $query[] = $pengeluaran;
        $query[] = $tabungan;
        $query[] = $sedekah;

        return $query;
    }

    public function getSumData($table,$column,$where,$valueWhere,$valueYear)
    {
        $query = $this->db->query("SELECT SUM(".$column.") as data FROM ".$table." WHERE MONTH(".$where.")=".$valueWhere." AND YEAR(".$where.")=".$valueYear." AND user_id=".$this->session->userdata('id_user')."");
        $row = $query->row_array();
        if ($row['data']==NULL) {
            return 0;
        } else {
            return (int)$row['data'];
        }
        
    }

    public function getDataPengeluaran($tipe,$month,$year)
    {
        $query = $this->db->query("SELECT SUM(total) as data FROM tb_pengeluaran WHERE MONTH(tgl)=".$month." AND YEAR(tgl)=".$year." AND type=".$tipe." AND user_id=".$this->session->userdata('id_user')."");
        $row = $query->row_array();
        if ($row['data']==NULL) {
            return 0;
        } else {
            return (int)$row['data'];
        }
    }
}

/* End of file Dashboard_m.php */
