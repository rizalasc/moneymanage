<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_m', 'auth');
    }


    public function page()
    {
        $data['title'] = "Login Page";
        $this->load->view('template/auth_header', $data);
        $this->load->view('auth/login');
        $this->load->view('template/auth_footer');
    }

    public function index()
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');

        if ($this->form_validation->run() == FALSE) {
            $this->page();
            $cookie = get_cookie('dtheme');
            if ($this->session->userdata('username')) {
                redirect('dashboard');
            } else if (($cookie != '') || ($cookie != null)) {
                $row = $this->auth->getByCookie($cookie);
                if ($row) {
                    $this->_session($row);
                }
            }
        } else {
            $this->_login();
        }
    }

    private function _session($row)
    {
        $data = [
            'username' => $row['username'],
            'id_user' => $row['id']
        ];
        $this->session->set_userdata($data);
        redirect('dashboard');
    }

    public function _login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $user = $this->auth->getUser($username);
        // var_dump($user);
        if ($user) {
            if (password_verify($password, $user['password'])) {
                $data = [
                    'username' => $user['username'],
                    'id_user'=>$user['id']
                ];
                $this->session->set_userdata($data);
                redirect('dashboard');
            } else {
                $this->session->set_flashdata('massage', '<div class="alert alert-danger" role="alert">Wrong password!</div>');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Username is not registered!</div>');
            redirect('auth');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">You have been logged out!</div>');

        redirect('auth');
    }

    public function blocked()
    {
        $this->load->view('auth/blocked');
    }

    public function forgotPassword()
    {
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = 'Forgot Password';
            $this->load->view('template/auth_header', $data);
            $this->load->view('auth/forgot-password');
            $this->load->view('template/auth_footer');
        } else {
            $username = $this->input->post('username');
            $user = $this->auth->getUser($username);

            if ($user) {
                $token = md5(uniqid(rand(0, 32), true));
                $user_token = [
                    'email' => $user['email'],
                    'token' => $token,
                    'created_at' => date('Y-m-d')
                ];
                $this->auth->insertToken($user_token);
                _sendEmail($user['email'], $token, 'forgot');
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Please check your email to reset your password!</div>');

                redirect('auth/forgotpassword');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email is not registered or activated!</div>');

                redirect('auth/forgotpassword');
            }
        }
    }

    public function regis()
    {
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[tb_user.email]');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|is_unique[tb_user.username]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[6]');

        
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = 'Registration';
            $this->load->view('template/auth_header', $data);
            $this->load->view('auth/register');
            $this->load->view('template/auth_footer');
        } else {
            $data = [
                'name' => $this->input->post('name'),
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'img' => 'default.jpg',
                'password' => password_hash($this->input->post('password'),PASSWORD_BCRYPT)
            ];
            $this->db->insert('tb_user',$data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Account has been created!</div>');
            redirect('auth');
        }
    }

    public function profile()
    {
        if ($this->session->userdata('username')) {
            $data['title'] = 'My Profile';
            $data['user'] = $this->auth->getUser($this->session->userdata('username'));
            _layout('auth/profile',$data);
        } else {
            
            redirect('auth','refresh');
            
        }
        
    }
    public function editprofile()
    {
        if ($this->session->userdata('username')) {
            $data['title'] = 'Edit Profile';
            $data['user'] = $this->auth->getUser($this->session->userdata('username'));

            _layout('auth/profile-edit', $data);

        } else {
            redirect('auth', 'refresh');
        }
    }

    public function edit()
    {
        $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[5]');
        $username = $this->session->userdata('username');
        $data['title'] = 'Edit Profile';
        $data['user'] = $this->auth->getUser($username);

        
        if ($this->form_validation->run() == FALSE) {
            _layout('auth/profile-edit',$data);
        } else {
            $uname = $this->input->post('name');
            $upload_image = $_FILES['image']['name'];
            // var_dump($upload_image);
            if ($upload_image) {
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = '1024';
                $config['upload_path'] = './assets/img/profile/';

                $this->load->library('upload',$config);
                if ($this->upload->do_upload('image')) {
                    $old_image = $data['user']['img'];
                    // var_dump($old_image);
                    if ($old_image != 'default.jpg') {
                        unlink(FCPATH . 'assets/img/profile/'.$old_image);
                    }
                    $new_image = $this->upload->data('file_name');
                    // var_dump($new_image);
                    $this->db->set('img',$new_image);
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">' . $this->upload->display_errors() . '</div>');
                }
                
            }
            
        }

        $this->db->set('name',$uname);
        $this->db->where('username',$username);
        $this->db->update('tb_user');
        
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Profil Anda telah diperbarui!</div>');

        redirect('auth/profile');
    }

    public function changepass()
    {
        $data['title'] = "Change Password";
        $data['user'] = $this->auth->getUser($this->session->userdata('username'));

        $this->form_validation->set_rules('new_password1', 'New Password', 'trim|required|min_length[6]|matches[new_password2]');
        $this->form_validation->set_rules('new_password2', 'Confirmation Password', 'trim|required|min_length[6]|matches[new_password1]');
        
        if ($this->form_validation->run() == FALSE) {
            _layout('auth/change',$data);
        } else {
            $newpassword = $this->input->post('new_password1');
            $passwordhash = password_hash($newpassword,PASSWORD_BCRYPT);
            
            $this->db->set('password',$passwordhash);
            $this->db->where('username',$this->session->userdata('username'));
            $this->db->update('tb_user');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Password telah diubah!</div>');

            redirect('dashboard');
            
        }
        
    
    }
}

/* End of file Auth.php */
