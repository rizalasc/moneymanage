<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_m', 'auth');
        $this->load->model('Dashboard_m','dashboard');
        is_logged_in();
    }


    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->auth->getUser($this->session->userdata('username'));
        $data['info'] = $this->dashboard->getInfo();
        $chart = $this->dashboard->getDataChart();
        $month = array();
        $pemasukan = array();
        $pengeluaran = array();
        $tabungan = array();
        $sedekah = array();
        // var_dump($this->dashboard->getSumData('tb_pemasukan','jumlah','waktu',7));
        $bln = $this->input->post('bulan');
        $thn = $this->input->post('tahun');
        if ($bln && $thn) {
            $year = $thn;
            $today = $bln;
            $bul = $this->monthName($today);
            $tahun = $year;
        } else {
            $year = date("Y");
            $today = date('n');
            $bul = $this->monthName($today);
            $tahun = $year;
        }
        $pengeluarantipe=array();
        for ($i=0; $i < 4; $i++) { 
            foreach ($chart[$i] as $c) {
                if (!in_array($this->monthName($c->month),$month)) {
                    $monthName = $this->monthName($c->month);
                    $pmsk = $this->dashboard->getSumData('tb_pemasukan','jumlah','waktu',$c->month,$year);
                    $peng = $this->dashboard->getSumData('tb_pengeluaran','total','tgl',$c->month,$year);
                    $tab = $this->dashboard->getSumData('tb_tabungan','jumlah','tgl_save',$c->month,$year);
                    $sed = $this->dashboard->getSumData('tb_sedekah','jumlah_sedekah','tgl_sedekah',$c->month,$year);
                    $pemasukan[] = $pmsk;
                    $pengeluaran[] = $peng;
                    $tabungan[] = $tab;
                    $sedekah[] = $sed;
                    $month[]=$monthName;
                }
            }
        }

        $pengeluarantipe[]= $this->dashboard->getDataPengeluaran(1,$today,$year);
        $pengeluarantipe[]= $this->dashboard->getDataPengeluaran(0,$today,$year);
        $bulanan = array();
        $bulanan[]=$this->dashboard->getSumData('tb_pemasukan','jumlah','waktu',$today,$year);
        $bulanan[]=$this->dashboard->getSumData('tb_pengeluaran','total','tgl',$today,$year);
        $bulanan[]=$this->dashboard->getSumData('tb_tabungan','jumlah','tgl_save',$today,$year);
        $bulanan[]=$this->dashboard->getSumData('tb_sedekah','jumlah_sedekah','tgl_sedekah',$today,$year);
        
        $data['tipe'] = json_encode($pengeluarantipe);
        $data['bulan'] = $bul;
        $data['tahun'] = $tahun;
        $data['labelBulanan'] = json_encode(array($this->monthName($today)));
        $data['bulanan'] = $bulanan;
        $data['months'] = json_encode($month);
        $data['pemasukan'] = json_encode($pemasukan);
        $data['pengeluaran'] = json_encode($pengeluaran);
        $data['tabungan'] = json_encode($tabungan);
        $data['sedekah'] = json_encode($sedekah);
        _layout('content/dashboard', $data);
        // if (!in_array(8,$month)) {
        //     print_r("Exist");
        // }
    }

    public function monthName($month)
    {
        switch ($month) {
            case 1:
                $m = "Januari";
                break;
            case 2:
                $m = "Februari";
                break;
            case 3:
                $m = "Maret";
                break;
            case 4:
                $m = "April";
                break;
            case 5:
                $m = "Mei";
                break;
            case 6:
                $m = "Juni";
                break;
            case 7:
                $m = "Juli";
                break;
            case 8:
                $m = "Agustus";
                break;
            case 9:
                $m = "September";
                break;
            case 10:
                $m = "Oktober";
                break;
            case 11:
                $m = "November";
                break;
            case 12:
                $m = "Desember";
                break;
            default:
                $m = "Input Invalid";
                break;
        }

        return $m;
    }
}

/* End of file Dashboard.php */
