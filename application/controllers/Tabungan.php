<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Tabungan extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Auth_m","auth");
        $this->load->model("Tabungan_m","tabungan");
        is_logged_in();
    }
    
    public function index()
    {
        $data["title"] = "Data Tabungan";
        $data["user"] = $this->auth->getUser($this->session->userdata("username"));
        _layout("pemasukan/tabungan",$data);
    }

    public function datatable()
    {
        $list = $this->tabungan->get_datatables();
        $data = array();
        foreach ($list as $key) {
            $row = array();
            $row[]=$key->id;
            $row[]=$key->jumlah;
            $row[]=$key->saving_place;
            $row[]=date("d M Y",strtotime($key->tgl_save));

            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_tabungan(' . "'" . $key->id . "'" . ')"><i class="fas fa-fw fa-edit"></i></a>
                  <a href="#hapus" class="trigger-btn" data-toggle="modal"><button type="button" onclick="delete_tabungan(' . "'" . $key->id . "'" . ')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>';

            $data[] = $row;

        }

        $result = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->tabungan->count_all(),
            "recordsFiltered" => $this->tabungan->count_filtered(),
            "data" => $data,
        );


        echo json_encode($result);
        exit();

    }

    public function add()
    {
        $data["title"] = "Data Tabungan";
        $data["user"] = $this->auth->getUser($this->session->userdata("username"));

        $this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
        $this->form_validation->set_rules('save_place', 'Tempat Simpan', 'trim|required');
        $this->form_validation->set_rules('tgl_save', 'Tanggal', 'trim|required');
        
        
        if ($this->form_validation->run() == FALSE) {
            _layout("pemasukan/tabungan",$data);
        } else {
            $simpan = [
                'jumlah'=>$this->input->post('jumlah'),
                'saving_place'=>$this->input->post('save_place'),
                'tgl_save'=>$this->input->post('tgl_save'),
                'user_id'=>$this->session->userdata('id_user')
            ];
            $this->tabungan->save($simpan);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data berhasil disimpan</div>');

            redirect('tabungan/add', 'refresh');
        }
    }
    public function update($id=NULL)
    {
        $data["title"] = "Data Tabungan";
        $data["user"] = $this->auth->getUser($this->session->userdata("username"));

        $this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
        $this->form_validation->set_rules('save_place', 'Tempat Simpan', 'trim|required');
        $this->form_validation->set_rules('tgl_save', 'Tanggal', 'trim|required');
        
        
        if ($this->form_validation->run() == FALSE) {
            _layout("pemasukan/tabungan",$data);
        } else {
            $update = [
                'jumlah'=>$this->input->post('jumlah'),
                'saving_place'=>$this->input->post('save_place'),
                'tgl_save'=>$this->input->post('tgl_save'),
                'user_id'=>$this->session->userdata('id_user')
            ];
            $this->tabungan->update($update,$id);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data berhasil disimpan</div>');

            redirect('tabungan/add', 'refresh');
        }
    }

    public function edit($id){
        $data = $this->tabungan->getById($id);
        echo json_encode($data);
    }

    public function delete($id)
    {
        $this->tabungan->delete($id);
        $this->session->set_flashdata('message', "<div class='alert alert-danger'>Berhasil Menghapus Data!</div>");
        redirect('tabungan', 'refresh');
    }

}

/* End of file Tabungan.php */
