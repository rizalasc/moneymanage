<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pemasukan extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('Auth_m', 'auth');
        $this->load->model("Pemasukan_m","pemasukan");
        is_logged_in();
    }

    public function index()
    {
        $data['title'] = "Data Pemasukan";
        $data['user'] = $this->auth->getUser($this->session->userdata('username'));
        _layout('pemasukan/index', $data);
    }

    public function datatable(){
        $list = $this->pemasukan->get_datatables();
        $data = array();
        foreach ($list as $key) {
            $row = array();
            $row[]=$key->id;
            $row[]=$key->jumlah;
            $row[]=$key->dari;
            $row[]=date("d M Y",strtotime($key->waktu));

            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_pemasukan(' . "'" . $key->id . "'" . ')"><i class="fas fa-fw fa-edit"></i></a>
                  <a href="#hapus" class="trigger-btn" data-toggle="modal"><button type="button" onclick="delete_pemasukan(' . "'" . $key->id . "'" . ')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>';

            $data[] = $row;

        }

        $result = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->pemasukan->count_all(),
            "recordsFiltered" => $this->pemasukan->count_filtered(),
            "data" => $data,
        );


        echo json_encode($result);
        exit();

    }

    public function add(){
        $data['title'] = "Data Pemasukan";
        $data['user'] = $this->auth->getUser($this->session->userdata('username'));

        $this->form_validation->set_rules('jumlah', 'Jumlah Uang', 'trim|required');
        $this->form_validation->set_rules('dari', 'Asal Uang', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        
        
        if ($this->form_validation->run() == FALSE) {
            _layout('pemasukan/index', $data);
            
        } else {
            $simpan = [
                'jumlah'=>$this->input->post('jumlah'),
                'dari'=>$this->input->post('dari'),
                'waktu'=>$this->input->post('tanggal'),
                'user_id'=>$this->session->userdata('id_user')
            ];
            $this->pemasukan->save($simpan);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data berhasil disimpan</div>');

            redirect('pemasukan/add', 'refresh');

        }
    }

    public function edit($id)
    {
        $data = $this->pemasukan->getById($id);
        echo json_encode($data);
    }

    public function update($id=NULL)
    {
        $data['title'] = "Data Pemasukan";
        $data['user'] = $this->auth->getUser($this->session->userdata('username'));

        $this->form_validation->set_rules('jumlah', 'Jumlah Uang', 'trim|required');
        $this->form_validation->set_rules('dari', 'Asal Uang', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        
        
        if ($this->form_validation->run() == FALSE) {
            _layout('pemasukan/index', $data);
            
        } else {
            $update = [
                'jumlah'=>$this->input->post('jumlah'),
                'dari'=>$this->input->post('dari'),
                'waktu'=>$this->input->post('tanggal'),
                'user_id'=>$this->session->userdata('id_user')
            ];
            $this->pemasukan->update($update,$id);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data berhasil disimpan</div>');

            redirect('pemasukan/add', 'refresh');

        }
    }

    public function delete($id)
    {
        $this->pemasukan->delete($id);
        $this->session->set_flashdata('message', "<div class='alert alert-danger'>Berhasil Menghapus Data!</div>");
        redirect('pemasukan', 'refresh');

    }

}

/* End of file Pemasukan.php */
