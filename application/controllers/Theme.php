<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Theme extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_m', 'auth');
        $this->load->model('Theme_m', 'theme');
        $this->load->model('Category_m', 'category');
        $this->load->model('Whatsapp_m', 'whatsapp');
    }

    public function index()
    {
        $data['title'] = "Data Theme";
        $data['user'] = $this->auth->getUser($this->session->userdata('username'));
        _layout('theme/data-theme', $data);
    }

    public function add()
    {
        $data['title'] = "Data Theme";
        $data['main_title'] = "Add Theme";
        $data['user'] = $this->auth->getUser($this->session->userdata('username'));
        $data['category'] = $this->category->getCategory();
        $data['whatsapp'] = $this->whatsapp->getWhatsapp();
        // var_dump($data['category']);
        _layout('theme/add-theme', $data);
    }

    public function theme()
    {
        $list = $this->theme->get_datatables();
        $data = array();
        foreach ($list as $theme) {
            $row = array();
            $row[] = $theme->id;
            $row[] = $theme->name_theme;
            $row[] = $theme->screen_shot1;
            $row[] = $theme->screen_shot2;
            $row[] = $theme->screen_shot3;
            $row[] = $theme->category;
            $row[] = $theme->file;
            $row[] = $theme->whatsapp_type;
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_kategori(' . "'" . $theme->id . "'" . ')"><i class="fas fa-fw fa-edit"></i></a>
                  <a href="#hapus" class="trigger-btn" data-toggle="modal"><button type="button" onclick="delete_kategori(' . "'" . $theme->id . "'" . ')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>';

            $data[] = $row;
        }

        $result = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->theme->count_all(),
            "recordsFiltered" => $this->theme->count_filtered(),
            "data" => $data,
        );


        echo json_encode($result);
        exit();
    }

    public function save(){
        $this->form_validation->set_rules('themeName', 'Theme Name', 'trim|required|min_length[5]');
        
        if ($this->form_validation->run() == FALSE) {
            $this->add();
        } else {
            $themeName = $this->input->post('themeName');
            $fileTheme = $_FILES['fileTheme']['name'];
            $screenshot1 = $_FILES['screenshot1']['name'];
            $screenshot2 = $_FILES['screenshot2']['name'];
            $screenshot3 = $_FILES['screenshot3']['name'];

            if (!is_dir('upload/theme/'.str_replace(" ","",$themeName))) {
                mkdir('./upload/theme/'.str_replace(" ","",$themeName), 0777, TRUE);
            }

            _uploadFile('fileTheme',$themeName);
            for ($i=1; $i <=3 ; $i++) { 
                $upload = _uploadImage("screenshot1",$themeName,$i);
                var_dump($upload);
            }
        }
        

    }
}

/* End of file Theme.php */
