<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Pengeluaran extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Pengeluaran_m","pengeluaran");
        $this->load->model("Auth_m","auth");
        is_logged_in();
    }
    

    public function index()
    {
        $data["title"]="Data Pengeluaran";
        $data["user"]=$this->auth->getUser($this->session->userdata("username"));
        _layout("pemasukan/pengeluaran",$data);
    }

    public function datatable(){
        $list = $this->pengeluaran->get_datatables();
        $data = array();
        foreach ($list as $key) {
            $row = array();
            $row[]=$key->id;
            $row[]=$key->nm_toko;
            $row[]=$key->nm_barang;
            $row[]=$key->type==0?"Tidak Tetap":"Tetap";
            $row[]=$key->qty;
            $row[]=$key->harga;
            $row[]=$key->total;
            $row[]=date("d M Y",strtotime($key->tgl));

            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_pengeluaran(' . "'" . $key->id . "'" . ')"><i class="fas fa-fw fa-edit"></i></a>
                  <a href="#hapus" class="trigger-btn" data-toggle="modal"><button type="button" onclick="delete_pengeluaran(' . "'" . $key->id . "'" . ')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>';

            $data[] = $row;
        }

        $result = array(
            "draw" => $_POST["draw"],
            "recordsTotal" => $this->pengeluaran->count_all(),
            "recordsFiltered" => $this->pengeluaran->count_filtered(),
            "data" => $data,
        );

        echo json_encode($result);
        exit();
    }

    public function add()
    {
        $data["title"] = "Data Pengeluaran";
        $data["user"] = $this->auth->getUser($this->session->userdata("username"));

        $this->form_validation->set_rules('nm_toko', 'Nama Toko', 'trim|required');
        $this->form_validation->set_rules('nm_barang', 'Nama Barang', 'trim|required');
        $this->form_validation->set_rules('qty', 'Quantity', 'trim|required');
        $this->form_validation->set_rules('type', 'Tipe', 'trim|required');
        $this->form_validation->set_rules('harga', 'Harga', 'trim|required');
        $this->form_validation->set_rules('tgl', 'Tanggal', 'trim|required');
        

        
        if ($this->form_validation->run() == FALSE) {
            _layout("pemasukan/pengeluaran",$data);
        } else {
            $simpan = [
                'nm_toko'=>$this->input->post('nm_toko'),
                'nm_barang'=>$this->input->post('nm_barang'),
                'qty'=>$this->input->post('qty'),
                'harga'=>$this->input->post('harga'),
                'total'=>$this->input->post('qty')*$this->input->post('harga'),
                'tgl'=>$this->input->post('tgl'),
                'user_id'=>$this->session->userdata('id_user'),
                'type'=>$this->input->post("type")
            ];
            $this->pengeluaran->save($simpan);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data berhasil disimpan</div>');

            redirect('pengeluaran/add', 'refresh');
        }
        
    }

    public function edit($id)
    {
        $data = $this->pengeluaran->getById($id);
        echo json_encode($data);
    }

    public function update($id=NULL)
    {
        $data["title"] = "Data Pengeluaran";
        $data["user"] = $this->auth->getUser($this->session->userdata("username"));

        $this->form_validation->set_rules('nm_toko', 'Nama Toko', 'trim|required');
        $this->form_validation->set_rules('nm_barang', 'Nama Barang', 'trim|required');
        $this->form_validation->set_rules('qty', 'Quantity', 'trim|required');
        $this->form_validation->set_rules('type', 'Tipe', 'trim|required');
        $this->form_validation->set_rules('harga', 'Harga', 'trim|required');
        $this->form_validation->set_rules('tgl', 'Tanggal', 'trim|required');
        

        
        if ($this->form_validation->run() == FALSE) {
            _layout("pemasukan/pengeluaran",$data);
        } else {
            $update = [
                'nm_toko'=>$this->input->post('nm_toko'),
                'nm_barang'=>$this->input->post('nm_barang'),
                'qty'=>$this->input->post('qty'),
                'harga'=>$this->input->post('harga'),
                'total'=>$this->input->post('qty')*$this->input->post('harga'),
                'tgl'=>$this->input->post('tgl'),
                'user_id'=>$this->session->userdata('id_user'),
                'type'=>$this->input->post("type")
            ];
            $this->pengeluaran->update($update,$id);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data berhasil disimpan</div>');

            redirect('pengeluaran/add', 'refresh');
        }
    }

    public function delete($id)
    {
        $this->pengeluaran->delete($id);
        $this->session->set_flashdata('message', "<div class='alert alert-danger'>Berhasil Menghapus Data!</div>");
        redirect('pengeluaran', 'refresh');

    }

}

/* End of file Pengeluaran.php */
