<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Sedekah extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Auth_m","auth");
        $this->load->model("Sedekah_m","sedekah");
        is_logged_in();
    }
    

    public function index()
    {
        $data["title"] = "Data Sedekah";
        $data["user"] = $this->auth->getUser($this->session->userdata("username"));
        _layout("pemasukan/sedekah",$data);
    }

    public function datatable()
    {
        $list = $this->sedekah->get_datatables();
        $data = array();
        foreach ($list as $key) {
            $row = array();
            $row[] = $key->id;
            $row[] = $key->jumlah_sedekah;
            $row[] = $key->dari;
            $row[] = $key->untuk;
            $row[] = date("d M Y",strtotime($key->tgl_sedekah));

            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_sedekah(' . "'" . $key->id . "'" . ')"><i class="fas fa-fw fa-edit"></i></a>
                  <a href="#hapus" class="trigger-btn" data-toggle="modal"><button type="button" onclick="delete_sedekah(' . "'" . $key->id . "'" . ')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>';

            $data[] = $row;

        }
        $result = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->sedekah->count_all(),
            "recordsFiltered" => $this->sedekah->count_filtered(),
            "data" => $data,
        );


        echo json_encode($result);
        exit();
    }

    public function add()
    {
        $data["title"] = "Data Sedekah";
        $data["user"] = $this->auth->getUser($this->session->userdata("username"));

        $this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
        $this->form_validation->set_rules('dari', 'Dari Siapa', 'trim|required');
        $this->form_validation->set_rules('untuk', 'Untuk Siapa', 'trim|required');
        $this->form_validation->set_rules('tgl', 'Tanggal', 'trim|required');
        
        
        if ($this->form_validation->run() == FALSE) {
            _layout("pemasukan/sedekah",$data);
        } else {
            $simpan = [
                'jumlah_sedekah'=>$this->input->post('jumlah'),
                'dari'=>$this->input->post('dari'),
                'untuk'=>$this->input->post('untuk'),
                'tgl_sedekah'=>$this->input->post('tgl'),
                'user_id'=>$this->session->userdata('id_user')
            ];
            $this->sedekah->save($simpan);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data berhasil disimpan</div>');

            redirect('sedekah/add', 'refresh');
        }
    }

    public function update($id=NULL)
    {
        $data["title"] = "Data Sedekah";
        $data["user"] = $this->auth->getUser($this->session->userdata("username"));

        $this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
        $this->form_validation->set_rules('dari', 'Dari Siapa', 'trim|required');
        $this->form_validation->set_rules('untuk', 'Untuk Siapa', 'trim|required');
        $this->form_validation->set_rules('tgl', 'Tanggal', 'trim|required');
        
        
        if ($this->form_validation->run() == FALSE) {
            _layout("pemasukan/sedekah",$data);
        } else {
            $update = [
                'jumlah_sedekah'=>$this->input->post('jumlah'),
                'dari'=>$this->input->post('dari'),
                'untuk'=>$this->input->post('untuk'),
                'tgl_sedekah'=>$this->input->post('tgl'),
                'user_id'=>$this->session->userdata('id_user')
            ];
            $this->sedekah->update($update,$id);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data berhasil disimpan</div>');

            redirect('sedekah/add', 'refresh');
        }
    }

    public function edit($id){
        $data = $this->sedekah->getById($id);
        echo json_encode($data);
    }

    public function delete($id)
    {
        $this->sedekah->delete($id);
        $this->session->set_flashdata('message', "<div class='alert alert-danger'>Berhasil Menghapus Data!</div>");
        redirect('sedekah', 'refresh');
    }

}

/* End of file Sedekah.php */
